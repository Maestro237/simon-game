var gamePattern = [];
var userClickedPattern = [];
var buttonColors = ["red", "blue", "green", "yellow"];
var level = 0;
var start = false;

$(".btn").click(function () {
    var userChosenColor = $(this).attr("id");
    userClickedPattern.push(userChosenColor);
    animatePress(userChosenColor);
    checkAnswer(userClickedPattern.length - 1);
    playSound(userChosenColor);
    //console.log(userClickedPattern);
});

function nextSequence(){
    var randomNumber = Math.floor(Math.random() * 4);
    var randomChosenColor = buttonColors[randomNumber];
    gamePattern.push(randomChosenColor);
    $("#"+randomChosenColor).fadeOut(300).fadeIn(300);
    playSound(randomChosenColor);
    $("h1").text("Level " + level);
    level++;

    //$("#"+randomChosenColor).fadeOut(300).fadeIn(300);
    //var music = new Audio("sounds/" + randomChosenColor + ".mp3");
    //music.play();

    //console.log(randomChosenColor);
}

function playSound(name){
    //var randomNumber = Math.floor(Math.random() * 4);
    //var randomChosenColor = buttonColors[randomNumber];
    //gamePattern.push(randomChosenColor);
    var music = new Audio("sounds/" + name + ".mp3");
    music.play();
}

function animatePress(currentColor){
    $("."+currentColor).addClass("pressed");
    setTimeout(() => {
        $("."+currentColor).removeClass("pressed");
    }, 100);
}

$(document).keypress(function () {
    if(!start){
        nextSequence();
        start = true;
    }
});

function checkAnswer(currentLevel){
    if(userClickedPattern[currentLevel] === gamePattern[currentLevel]){
        console.log("success!");

        if(userClickedPattern.length=== gamePattern.length){
            console.log("sequence correct!");
            setTimeout(() => {
                nextSequence();
                userClickedPattern = [];
            }, 1000);
        }
    }else{
        $("body").addClass("game-over");
        $("h1").text("Game Over ! Press Any Key to Restart !");
        setTimeout(() => {
           $("body").removeClass("game-over"); 
        }, 200);
        startOver();
        console.log("wrong!");
    }
}

function startOver(){
    level = 0;
    gamePattern = [];
    userClickedPattern = [];
    start = false;
}
